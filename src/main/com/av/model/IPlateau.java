package com.av.model;

public interface IPlateau {
	boolean isValidRange(IPositionCoordinates posCoordinates);
}