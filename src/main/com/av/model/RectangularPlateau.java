package com.av.model;

public class RectangularPlateau implements IPlateau {
	private final IPositionCoordinates bottomLeftCoordinates;
	private final IPositionCoordinates topRightCoordinates;

	public RectangularPlateau(IPositionCoordinates topRightCoordinates) {
		this(new PositionCoordinates(0, 0), topRightCoordinates);
	}

	public RectangularPlateau(IPositionCoordinates bottomLeftCoordinates, IPositionCoordinates topRightCoordinates) {
		this.bottomLeftCoordinates = bottomLeftCoordinates;
		this.topRightCoordinates = topRightCoordinates;
	}

	@Override
	public boolean isValidRange(IPositionCoordinates posCoordinates) {
		boolean xCoordinateNotLessThanBottom = posCoordinates.getXCoordinate() >= bottomLeftCoordinates.getXCoordinate();
		boolean yCoordinateNotLessThanBottom = posCoordinates.getYCoordinate() >= bottomLeftCoordinates.getYCoordinate();
		
		boolean xCoordinateNotMoreThanTop = posCoordinates.getXCoordinate() <= topRightCoordinates.getXCoordinate();
		boolean yCoordinateNotMoreThanTop = posCoordinates.getYCoordinate() <= topRightCoordinates.getYCoordinate();
		
		return xCoordinateNotLessThanBottom && yCoordinateNotLessThanBottom && xCoordinateNotMoreThanTop && yCoordinateNotMoreThanTop;
	}
}
