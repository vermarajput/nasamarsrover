package com.av.model;

public class PositionCoordinates implements IPositionCoordinates {
	private final int xCoordinate;
	private final int yCoordinate;

	public PositionCoordinates(int xCoordinate, int yCoordinate) {
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
	}

	@Override
	public int getXCoordinate() {
		return this.xCoordinate;
	}

	@Override
	public int getYCoordinate() {
		return this.yCoordinate;
	}

	@Override
	public String toString() {
		return xCoordinate + " " + yCoordinate;
	}
}
