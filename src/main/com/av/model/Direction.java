package com.av.model;

import java.util.Arrays;

public enum Direction {

    N(0,1) {
        @Override
        public Direction leftSide() {
            return W;
        }

        @Override
        public Direction rightSide() {
            return E;
        }
    },

    S(0,-1) {
        @Override
        public Direction rightSide() {
            return W;
        }

        @Override
        public Direction leftSide() {
            return E;
        }
    },

    E(1,0) {
        @Override
        public Direction rightSide() {
            return S;
        }

        @Override
        public Direction leftSide() {
            return N;
        }
    },

    W(-1,0) {
        @Override
        public Direction rightSide() {
            return N;
        }

        @Override
        public Direction leftSide() {
            return S;
        }
    };

    private final int xIncrement;
    private final int yIncrement;

    Direction(final int xIncrement, final int yIncrement) {
        this.xIncrement = xIncrement;
        this.yIncrement = yIncrement;
    }

    public abstract Direction rightSide();
    public abstract Direction leftSide();

    public int xIncrement() {
        return this.xIncrement;
    }

    public int yIncrement() {
        return this.yIncrement;
    }
    
    public static Direction fromString(String input) {
		if (input != null) {
			for (Direction dir : Direction.values()) {
				if (input.equalsIgnoreCase(dir.toString())) {
					return dir;
				}
			}
		}
		throw new IllegalArgumentException("Supplied input [" + input + "] is not a valid Direction. Valid directions are :" + names());
	}
    
    public static String names() {
        Direction[] dirs = values();
        String[] names = new String[dirs.length];

        for (int i = 0; i < dirs.length; i++) {
            names[i] = dirs[i].name();
        }

        return Arrays.toString(names);
    }
}