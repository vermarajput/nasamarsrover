package com.av.model;

import java.util.List;

import com.av.parser.IInstructionExecutor;
import com.av.parser.InstructionsParser;

/**
 * @author Aman Verma
 */
public class NasaRover {
	private IPlateau plateau;
	private Direction direction;
	private IPositionCoordinates posCoordinates;

	public NasaRover(IPlateau plateau, 
			IPositionCoordinates posCoordinates, 
			Direction direction) {
		this.plateau = plateau;
		this.posCoordinates = posCoordinates;
		this.direction = direction;
	}

	public void spin90DegreeLeft() {
		direction = direction.leftSide();
	}

	public void spin90DegreeRight() {
		direction = direction.rightSide();
	}

	public void move() {
		IPositionCoordinates updatedCoordinates = new PositionCoordinates(
				direction.xIncrement() + posCoordinates.getXCoordinate(), 
				direction.yIncrement() + posCoordinates.getYCoordinate());
		
		if(plateau.isValidRange(updatedCoordinates))
			posCoordinates = updatedCoordinates;
	}

	public void executeInstructions(final InstructionsParser instructionsParser) {
        List<IInstructionExecutor> instructionsExecutors = instructionsParser.toInstructionExecutors();
        for (IInstructionExecutor instructionExecutor : instructionsExecutors) {
        	instructionExecutor.insruct(this);
        }
    }
	
	public String getCurrentState(){
		return posCoordinates.toString() + " " + direction.toString();
 	}
}