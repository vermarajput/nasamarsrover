package com.av.model;

public interface IPositionCoordinates {

	public int getXCoordinate();

	public int getYCoordinate();
}