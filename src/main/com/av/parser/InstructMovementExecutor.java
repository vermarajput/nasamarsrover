package com.av.parser;

import com.av.model.NasaRover;

public class InstructMovementExecutor implements IInstructionExecutor {

	@Override
	public void insruct(NasaRover nasaRover) {
		nasaRover.move();
	}
}
