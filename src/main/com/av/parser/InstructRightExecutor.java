package com.av.parser;

import com.av.model.NasaRover;

public class InstructRightExecutor implements IInstructionExecutor {

	@Override
	public void insruct(NasaRover nasaRover) {
		nasaRover.spin90DegreeRight();
	}
}
