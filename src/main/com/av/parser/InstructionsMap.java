package com.av.parser;

import java.util.Arrays;

public enum InstructionsMap {

    L {
		@Override
		public IInstructionExecutor getInstructionExecutor() {
			return new InstructLeftExecutor();
		}
    },

    R {
		@Override
		public IInstructionExecutor getInstructionExecutor() {
			return new InstructRightExecutor();
		}
    },

    M {
		@Override
		public IInstructionExecutor getInstructionExecutor() {
			return new InstructMovementExecutor();
		}
    	
    };

    public abstract IInstructionExecutor getInstructionExecutor();
    
    public static InstructionsMap fromString(String input) {
		if (input != null) {
			for (InstructionsMap im : InstructionsMap.values()) {
				if (input.equalsIgnoreCase(im.toString())) {
					return im;
				}
			}
		}
		throw new IllegalArgumentException("Supplied input [" + input + "] is not a valid Instruction. Valid instructions are :" + names());
	}
    
    public static String names() {
        InstructionsMap[] insMap = InstructionsMap.values();
        String[] names = new String[insMap.length];

        for (int i = 0; i < insMap.length; i++) {
            names[i] = insMap[i].name();
        }

        return Arrays.toString(names);
    }
}