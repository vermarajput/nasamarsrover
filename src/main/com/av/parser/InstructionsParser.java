package com.av.parser;

import java.util.ArrayList;
import java.util.List;

public class InstructionsParser {

	private String instructions;

	public InstructionsParser(final String instructions) {
		this.instructions = instructions;
	}

	public List<IInstructionExecutor> toInstructionExecutors() {
		if (null == instructions || instructions.trim().isEmpty())
			return new ArrayList<IInstructionExecutor>();

		List<IInstructionExecutor> instructionExecutors = new ArrayList<IInstructionExecutor>();

		//for (String instruction : Arrays.copyOfRange(instructions.split(""), 1, instructions.length() + 1)) {
		// .split is not compatible in Java8 and Java7
		for(int i=0; i<instructions.length(); i++) {
			String instruction = instructions.substring(i, i+1);
			if (instruction == null)
				break;

			IInstructionExecutor mappedInstructions = InstructionsMap.fromString(instruction).getInstructionExecutor();
			instructionExecutors.add(mappedInstructions);
		}

		return instructionExecutors;
	}
}
