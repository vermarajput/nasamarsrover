package com.av.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.av.model.Direction;
import com.av.model.IPlateau;
import com.av.model.NasaRover;
import com.av.model.PositionCoordinates;
import com.av.model.RectangularPlateau;
import com.av.parser.InstructionsParser;
import com.av.util.InputValidator;

/**
 * Entry point of the program. 
 * 
 * @assumptions
 * 		Rover moving outside the periphery of Plateau then it will remain at same position, without moving
 * 		Rover positioned at coordinates outside periphery of Plateau be inert to all the commands and remain stationary: No movement, no turning
 * 		Invalid inputs in the supplied file will result in IllegalArgumentException thrown with appropriate error message.
 * 
 * @author Aman Verma
 *
 */

public class NasaController {
	public static void main(String [] args){
		 if(args.length < 1)
			 throw new IllegalArgumentException("Supplied file name is not a valid path or is not readable.");
		 
		 parseData(args[0]); 
	}
	
	public static void parseData(String fileName) {
		try (BufferedReader input = new BufferedReader(new FileReader(fileName))){

			String line;
			if ((line = input.readLine()) != null) {

				String[] topRightCoordinates = line.split(" ");
				// vaildate
				if (topRightCoordinates.length < 2)
					throw new IllegalArgumentException("Invalid input supplied [" + line + "]. Expected 2 Integers separated by a space character.");

				int xCoordinate = InputValidator.validateAndGetInteger(topRightCoordinates[0]);
				int yCoordinate = InputValidator.validateAndGetInteger(topRightCoordinates[1]);

				IPlateau plateau = new RectangularPlateau(new PositionCoordinates(xCoordinate, yCoordinate));

				while ((line = input.readLine()) != null) {

					String[] currentState = line.split(" ");
					// vaildate
					if (currentState.length < 3)
						throw new IllegalArgumentException("Invalid input supplied [" + line + "]. Expected 2 Integers separated by a space character, followed by Direction string.");

					int xCurCoordinate = InputValidator.validateAndGetInteger(currentState[0]);
					int yCurCoordinate = InputValidator.validateAndGetInteger(currentState[1]);
					String direction = InputValidator.validateAndGetString(currentState[2]);

					NasaRover nasaRover = new NasaRover(plateau, new PositionCoordinates(xCurCoordinate, yCurCoordinate), Direction.fromString(direction));

					if ((line = input.readLine()) != null) {
						nasaRover.executeInstructions(new InstructionsParser(line));
					}

					System.out.println(nasaRover.getCurrentState());
				}
			}
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Input is not in valid format. " + nfe.getMessage());
		} catch (IOException e) {
			throw new IllegalArgumentException(
					"Supplied file name " + fileName + " is not a valid path or is not readable. " + e.getMessage());
		}
	}
}
