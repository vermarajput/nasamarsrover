package com.av.util;

public class InputValidator {
	public static String validateAndGetString(String input){
		if(null == input || input.trim().isEmpty())
			throw new IllegalArgumentException("Supplied input is either null or empty.");
		return input;
	}

	public static Integer validateAndGetInteger(String input){
		if(null == input || input.trim().isEmpty())
			throw new IllegalArgumentException("Supplied input is either null or empty.");
		
		try{
			return Integer.parseInt(input);
		} catch (Exception e) {
			throw new IllegalArgumentException("[" + input + "] is not a valid Input. Expected integer. ", e );
		}
	}
}
