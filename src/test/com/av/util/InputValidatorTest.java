package com.av.util;

import org.junit.Assert;
import org.junit.Test;

public class InputValidatorTest {

	@Test
	public void testValidString() {
		Assert.assertEquals("ABC", InputValidator.validateAndGetString("ABC"));
	}

	@Test (expected=IllegalArgumentException.class)
	public void testEmptyString() {
		InputValidator.validateAndGetString("");
	}

	@Test (expected=IllegalArgumentException.class)
	public void testNullString() {
		InputValidator.validateAndGetString(null);
	}
	@Test
	public void testValidInteger() {
		Assert.assertEquals(new Integer(123), InputValidator.validateAndGetInteger("123"));
	}

	@Test (expected=IllegalArgumentException.class)
	public void testInvalidInteger() {
		InputValidator.validateAndGetInteger("AV");
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testEmptyInteger() {
		InputValidator.validateAndGetInteger("");
	}

	@Test (expected=IllegalArgumentException.class)
	public void testNullInteger() {
		InputValidator.validateAndGetInteger(null);
	}
}
