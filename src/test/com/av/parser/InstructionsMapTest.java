package com.av.parser;

import org.junit.Assert;
import org.junit.Test;

public class InstructionsMapTest {

	@Test
	public void testLeftInstructionsClass(){
		InstructionsMap im = InstructionsMap.L;
		Assert.assertTrue(im.getInstructionExecutor() instanceof InstructLeftExecutor);
		Assert.assertTrue(im.getInstructionExecutor().getClass().getName().equals(InstructLeftExecutor.class.getName()));
	}

	@Test
	public void testRightInstructionsClass(){
		InstructionsMap im = InstructionsMap.R;
		Assert.assertTrue(im.getInstructionExecutor() instanceof InstructRightExecutor);
	}
	
	@Test
	public void testMoveInstructionsClass(){
		InstructionsMap im = InstructionsMap.M;
		Assert.assertTrue(im.getInstructionExecutor() instanceof InstructMovementExecutor);
	}
}