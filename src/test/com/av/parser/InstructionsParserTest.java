package com.av.parser;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class InstructionsParserTest {
    @Test
    public void stringLMapsToRotateLeftCommand() {
        InstructionsParser parser = new InstructionsParser("L");
        List<IInstructionExecutor> ins = parser.toInstructionExecutors();

        Assert.assertTrue(ins.get(0) instanceof InstructLeftExecutor);
        Assert.assertEquals(1, ins.size());
    }

    @Test
    public void stringRMapsToRotateRightCommand() {
        InstructionsParser parser = new InstructionsParser("R");
        List<IInstructionExecutor> ins = parser.toInstructionExecutors();

        Assert.assertTrue(ins.get(0) instanceof InstructRightExecutor);
    }

    @Test
    public void stringMMapsToMoveCommand() {
        InstructionsParser parser = new InstructionsParser("M");
        List<IInstructionExecutor> ins = parser.toInstructionExecutors();

        Assert.assertTrue(ins.get(0) instanceof InstructMovementExecutor);
    }

	@Test
	public void emptyStringResultsInEmptyCommandList() {
		InstructionsParser parser = new InstructionsParser("");
		List<IInstructionExecutor> ins = parser.toInstructionExecutors();

		Assert.assertEquals(0, ins.size());
	}

	@Test
	public void nullStringResultsInEmptyCommandList() {
		InstructionsParser parser = new InstructionsParser(null);
		List<IInstructionExecutor> ins = parser.toInstructionExecutors();

		Assert.assertEquals(0, ins.size());
	}

	@Test
	public void stringToCommandMappingIsCaseInsensitive() {
		InstructionsParser parser = new InstructionsParser("mM");
		List<IInstructionExecutor> ins = parser.toInstructionExecutors();

		Assert.assertTrue(ins.get(0) instanceof InstructMovementExecutor);
		Assert.assertTrue(ins.get(1) instanceof InstructMovementExecutor);
	}

	@Test
	public void multiCommandStringIsMappedToCommandsInSameOrder() {
		InstructionsParser parser = new InstructionsParser("MRL");
		List<IInstructionExecutor> ins = parser.toInstructionExecutors();

		Assert.assertTrue(ins.get(0) instanceof InstructMovementExecutor);
		Assert.assertTrue(ins.get(1) instanceof InstructRightExecutor);
		Assert.assertTrue(ins.get(2) instanceof InstructLeftExecutor);
	}
}
