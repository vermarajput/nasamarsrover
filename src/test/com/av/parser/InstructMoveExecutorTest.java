package com.av.parser;

import org.junit.Assert;
import org.junit.Test;

import com.av.model.Direction;
import com.av.model.IPlateau;
import com.av.model.IPositionCoordinates;
import com.av.model.NasaRover;
import com.av.model.PositionCoordinates;
import com.av.model.RectangularPlateau;

public class InstructMoveExecutorTest {

    @Test
    public void testMoveInstruction() {
    	IInstructionExecutor ins = new InstructMovementExecutor();
        IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5,5));
        IPositionCoordinates startingPosition = new PositionCoordinates(1,2);
        NasaRover nasaRover = new NasaRover(plateau, startingPosition, Direction.N);
        ins.insruct(nasaRover);

        Assert.assertEquals("1 3 N", nasaRover.getCurrentState());
    }
}
