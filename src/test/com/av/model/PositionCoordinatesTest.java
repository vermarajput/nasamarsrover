package com.av.model;

import org.junit.Assert;
import org.junit.Test;

public class PositionCoordinatesTest {

	@Test
	public void testGetXCoordinate() {
		IPositionCoordinates positionCoordinates = new PositionCoordinates(2, 5);

		Assert.assertTrue(positionCoordinates.getXCoordinate() == 2);
		Assert.assertFalse(positionCoordinates.getXCoordinate() == 9);
	}
	
	@Test
	public void testGetYCoordinate() {
		IPositionCoordinates positionCoordinates = new PositionCoordinates(2, 5);

		Assert.assertTrue(positionCoordinates.getYCoordinate() == 5);
		Assert.assertFalse(positionCoordinates.getYCoordinate() == 2);
	}
}
