package com.av.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RectangularPlateauTest {

	private MockPositionCoordinates mockPosCoordTopRight, mockUpdatedPosCoord;
	@Before
	public void setup(){
		mockPosCoordTopRight = new MockPositionCoordinates();
		mockUpdatedPosCoord = new MockPositionCoordinates();
	}

	@Test
	public void testValidXRange() {
		mockPosCoordTopRight.setxCoordinate(5);
		mockPosCoordTopRight.setyCoordinate(5);
		RectangularPlateau rp  = new RectangularPlateau(mockPosCoordTopRight);
		
		mockUpdatedPosCoord.setxCoordinate(2);
		mockUpdatedPosCoord.setyCoordinate(5);
	
		Assert.assertTrue(rp.isValidRange(mockUpdatedPosCoord));
	}

	@Test
	public void testInValidXRange() {
		mockPosCoordTopRight.setxCoordinate(5);
		mockPosCoordTopRight.setyCoordinate(5);
		RectangularPlateau rp  = new RectangularPlateau(mockPosCoordTopRight);
		
		mockUpdatedPosCoord.setxCoordinate(6);
		mockUpdatedPosCoord.setyCoordinate(5);
	
		Assert.assertFalse(rp.isValidRange(mockUpdatedPosCoord));
	}

	@Test
	public void testValidYRange() {
		mockPosCoordTopRight.setxCoordinate(5);
		mockPosCoordTopRight.setyCoordinate(5);
		RectangularPlateau rp  = new RectangularPlateau(mockPosCoordTopRight);
		
		mockUpdatedPosCoord.setxCoordinate(2);
		mockUpdatedPosCoord.setyCoordinate(5);
	
		Assert.assertTrue(rp.isValidRange(mockUpdatedPosCoord));
	}

	@Test
	public void testInValidYRange() {
		mockPosCoordTopRight.setxCoordinate(5);
		mockPosCoordTopRight.setyCoordinate(5);
		RectangularPlateau rp  = new RectangularPlateau(mockPosCoordTopRight);
		
		mockUpdatedPosCoord.setxCoordinate(2);
		mockUpdatedPosCoord.setyCoordinate(6);
	
		Assert.assertFalse(rp.isValidRange(mockUpdatedPosCoord));
	}

}
