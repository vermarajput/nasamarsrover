package com.av.model;

import org.junit.Assert;
import org.junit.Test;

public class DirectionTest {

	@Test
	public void westIsOnleftSideOfNorth() {
		Direction north = Direction.N;
		Direction west = north.leftSide();

		Assert.assertEquals(Direction.W, west);
	}

	@Test
	public void eastIsOnRightOfNorth() {
		Direction north = Direction.N;
		Direction east = north.rightSide();

		Assert.assertEquals(Direction.E, east);
	}

	@Test
	public void northIsOnRightOfWest() {
		Direction west = Direction.W;
		Direction north = west.rightSide();

		Assert.assertEquals(Direction.N, north);
	}

	@Test
	public void southIsOnleftSideOfWest() {
		Direction west = Direction.W;
		Direction south = west.leftSide();

		Assert.assertEquals(Direction.S, south);
	}

	@Test
	public void eastIsOnleftSideOfSouth() {
		Direction south = Direction.S;
		Direction east = south.leftSide();

		Assert.assertEquals(Direction.E, east);
	}

	@Test
	public void westIsOnRightOfSouth() {
		Direction south = Direction.S;
		Direction west = south.rightSide();

		Assert.assertEquals(Direction.W, west);
	}

	@Test
	public void northIsOnleftSideOfEast() {
		Direction east = Direction.E;
		Direction north = east.leftSide();

		Assert.assertEquals(Direction.N, north);
	}

	@Test
	public void southIsOnRightOfEast() {
		Direction east = Direction.E;
		Direction south = east.rightSide();

		Assert.assertEquals(Direction.S, south);
	}

	@Test
	public void eastIsOneStepForwardOnXAxis() {
		Direction east = Direction.E;
		int incrementVal = east.xIncrement();

		Assert.assertEquals(1, incrementVal);
	}

	@Test
	public void eastIsStationaryOnYAxis() {
		Direction east = Direction.E;
		int incrementVal = east.yIncrement();

		Assert.assertEquals(0, incrementVal);
	}

	@Test
	public void westIsOneStepBackOnXAxis() {
		Direction west = Direction.W;
		int incrementVal = west.xIncrement();

		Assert.assertEquals(-1, incrementVal);
	}

	@Test
	public void westIsStationaryOnYAxis() {
		Direction west = Direction.W;
		int incrementVal = west.yIncrement();

		Assert.assertEquals(0, incrementVal);
	}

	@Test
	public void northIsOneStepForwardOnYAxis() {
		Direction north = Direction.N;
		int incrementVal = north.yIncrement();

		Assert.assertEquals(1, incrementVal);
	}

	@Test
	public void northIsStationaryOnXAxis() {
		Direction north = Direction.N;
		int incrementVal = north.xIncrement();

		Assert.assertEquals(0, incrementVal);
	}

	@Test
	public void southIsOneStepBackOnYAxis() {
		Direction south = Direction.S;
		int incrementVal = south.yIncrement();

		Assert.assertEquals(-1, incrementVal);
	}

	@Test
	public void southIsStationaryOnXAxis() {
		Direction south = Direction.S;
		int incrementVal = south.xIncrement();

		Assert.assertEquals(0, incrementVal);
	}
}