package com.av.model;

import org.junit.Assert;
import org.junit.Test;

import com.av.parser.InstructionsParser;

public class NasaRoverTest {

	@Test
	public void testGetCurrentState() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(3, 3);

		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		Assert.assertEquals("3 3 N", marsRover.getCurrentState());
	}

	@Test
	public void testSpin90DegreeLeft() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.spin90DegreeLeft();

		Assert.assertEquals("1 2 W", marsRover.getCurrentState());
	}

	@Test
	public void testSpin90DegreeRight() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.spin90DegreeRight();

		Assert.assertEquals("1 2 E", marsRover.getCurrentState());
	}

	@Test
	public void testMove() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.move();

		Assert.assertEquals("1 3 N", marsRover.getCurrentState());
	}

	@Test
	public void testSpin90DegreeRightInstruction() {

		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.executeInstructions(new InstructionsParser("R"));

		Assert.assertEquals("1 2 E", marsRover.getCurrentState());
	}

	@Test
	public void testSpin90DegreeLeftInstruction() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.executeInstructions(new InstructionsParser("L"));

		Assert.assertEquals("1 2 W", marsRover.getCurrentState());
	}

	@Test
	public void testMoveInstruction() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(1, 2);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.executeInstructions(new InstructionsParser("M"));

		Assert.assertEquals("1 3 N", marsRover.getCurrentState());
	}

	@Test
	public void testMultipleInstruction() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(3, 3);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.E);

		marsRover.executeInstructions(new InstructionsParser("MMRMMRMRRM"));

		Assert.assertEquals("5 1 E", marsRover.getCurrentState());
	}

	@Test
	public void testPlateauRange() {
		IPlateau plateau = new RectangularPlateau(new PositionCoordinates(5, 5));
		IPositionCoordinates startingPosition = new PositionCoordinates(3, 3);
		NasaRover marsRover = new NasaRover(plateau, startingPosition, Direction.N);

		marsRover.executeInstructions(new InstructionsParser("MMMMMMMMMMR"));

		Assert.assertEquals("3 5 E", marsRover.getCurrentState());
	}
}
